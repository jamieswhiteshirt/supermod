package glenn.supermod;

import net.minecraft.init.Blocks;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = SuperMod.MODID, name = "SuperMod", version = SuperMod.VERSION)
public class SuperMod
{
	public static final String MODID = "supermod";
	public static final String VERSION = "1.0";
	
	@Instance("supermod")
	static SuperMod instance;
	
	@SidedProxy(modId = SuperMod.MODID, clientSide = "glenn.supermod.client.ClientProxy", serverSide = "glenn.supermod.server.ServerProxy")
	public static CommonProxy proxy;
	
	public static SuperModConfigurations configurations;
	
	
	
	private void initBlocks()
	{
		
	}
	
	private void initItems()
	{
		
	}
	
	
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		configurations = new SuperModConfigurations(event.getSuggestedConfigurationFile());
		
		initBlocks();
		initItems();
		
		proxy.registerEventHandlers();
	}
	
	@EventHandler
	public void init(FMLInitializationEvent event)
	{
		// some example code
		System.out.println(configurations.whatToPrint);
		
		proxy.registerRenderers();
	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent event)
	{
		
	}
}