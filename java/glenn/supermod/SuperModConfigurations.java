package glenn.supermod;

import java.io.File;

import glenn.supermod.util.Configurations;

public class SuperModConfigurations extends Configurations
{
	public String whatToPrint;
	
	public SuperModConfigurations(File configurationsFile)
	{
		super(configurationsFile);
	}

	@Override
	protected void setDefaults()
	{
		whatToPrint = "";
	}

	@Override
	protected void onLoaded()
	{
		
	}

}